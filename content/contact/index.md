---
title: "Kontakt"
date: 2024-06-22T19:53:35+02:00
draft: false
---

Hallo liebe Freunde!
Habt ihr Fragen zu meinem Blog oder wollt ihr mehr über mich wissen? Vielleicht habt ihr auch Lob, Kritik oder tolle Verbesserungsvorschläge? Dann seid ihr hier genau richtig!

Ihr könnt mir auf zwei Arten schreiben:

- **Per E-Mail**: Schickt mir eine Nachricht an <email>liam[dot]albrant[at]proton.me</email>.
- **Bei Mastodon**: Findet mich unter `@liam_a@mastodon.social`.

Ich antworte nicht jeden Tag, aber ich versuche, alle Nachrichten zu lesen und zu beantworten. Also, keine Sorge, ich melde mich bei euch!
Freue mich auf eure Nachrichten!