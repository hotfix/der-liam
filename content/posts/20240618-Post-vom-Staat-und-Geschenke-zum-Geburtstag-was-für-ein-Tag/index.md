---
title: "Post Vom Staat Und Geschenke Zum Geburtstag - Was Für Ein Tag!"
date: 2024-06-18T20:13:11+02:00
draft: false
---

Heute Morgen war gleich doppelt Postzeit! Als ich gerade an Mamas Brust lag, kam der Postbote vorbei und brachte uns Briefe und ein Paket. :envelope:

Einer der Briefe war sogar für mich! Mein erster Brief von der Rentenversicherung.  Papa meinte scherzhaft, dass er eigentlich als erstes den Brief mit meiner Steuer-ID erwartet hätte.  
Na ja, dann zahle ich halt erst mal keine Steuern. Ist mir auch recht! Ob ich wohl schon Rentenanspruch habe, wenn ich die Rentenversicherungsnummer jetzt schon bekommen habe?

Das Paket war übrigens auch für mich.  Es war ein Geschenk von Kaufland zu meinem Geburtstag. Papa hat mich ja vor der Geburt bei dem Kaufland FamilienMomente Club angemeldet.  
Es werden wohl noch ein paar weitere Geschenke kommen, da bin ich total gespannt!

In diesem Paket waren ganz tolle Sachen dabei:

- Ein neues Badehandtuch mit meinem Namen drauf. Jetzt habe ich schon mal zwei, das ist voll cool!
- Eine Windel zum Probieren. Die ist mir jetzt aber noch etwas zu groß.
- Eine Packung mit Feuchttüchern, damit Papa und Mama mein sanftes Popo sauber machen können, wenn ich meine Windel voll gemacht habe.
- Eine Messlatte als Aufkleber. So kann ich dann immer schauen, wie groß ich geworden bin, wenn ich laufen kann.
- Für Mama gab's eine Packung Studentenfutter und ein paar [Lansinoh Stilleinlagen](https://amzn.to/4bhycir)&midast; (Mama findet diese besser als die von dm).

![Erste Box von Kaufland FamilienMomente Club 2024](Kaufland-FamilienMomente-Club-erstes-Geschenk.jpg)

Es ist so schön, so viele Geschenke zu bekommen!

Nachmittags sind wir wieder spazieren gegangen.  Diesmal war ich aber von Anfang an schon wach.  Mama und Papa sagten, sie wollten mal schauen, ob ich während des Spaziergangs einschlafe.
Ich konnte aber leider nicht einschlafen.  Wahrscheinlich nehme ich aktuell so viele neue Informationen auf, dass ich alles erstmal verarbeiten muss und dabei nicht einschlafen kann.
Irgendwann wurde ich dann doch müde und habe angefangen zu weinen.  Mama und Papa sind dann schnell wieder nach Hause gefahren.

Jetzt werde ich versuchen, ins Bett zu gehen, um meinen beiden lieben Eltern auch etwas Schlaf zu ermöglichen.

Ich hoffe, ich schaffe es heute und enttäusche die beiden nicht!