---
title: "Eine Woche Voller Sonnenschein, Sport Und Familienzeit"
date: 2024-07-21T20:36:08+02:00
draft: false
---

Was für eine Woche! ☀️ Die Sonne hat fast jeden Tag vom Himmel gelacht und uns mit Wärme und guter Laune versorgt. Es gab jede Menge zu erleben und zu entdecken. Lasst mich euch von meinen Abenteuern erzählen!

Die Woche begann ganz besonders für meinen Papa. Nach einer langen Pause konnte er endlich wieder zum Karate-Training gehen.  Ich finde es toll, dass er sich so fit hält!

Am Wochenende hatten wir Besuch von unseren Freunden Luba und Stefan mit ihrem kleinen Sohn Ben.  
Ben ist erst zwei Jahre alt und voller Energie. Er wollte die ganze Zeit mit mir spielen, aber leider bin ich noch zu klein.  
Trotzdem hatten wir viel Spaß zusammen. Ich hoffe, wir werden gute Freunde, wenn ich größer bin!

Nachmittags sind wir alle zusammen zum See gefahren. Ben hat im Wasser gespielt und ich habe mit Papa auf einer Decke gelegen und den Himmel beobachtet. ☁️ 
Es war so entspannend! Leider mussten wir schon bald wieder nach Hause fahren, weil Ben müde wurde.

Heute haben wir einen Ausflug an die Ostsee gemacht.  Oma und Onkel Pawel waren auch dabei. 
Es war ein wunderschöner Tag! ☀️ Alle außer Mama haben im Meer gebadet. Ich habe die ganze Zeit geschlafen. 
Die frische Meeresluft tut so gut!  Wir waren erst am späten Nachmittag wieder zu Hause. Es war so schön, Oma und Onkel Pawel zu sehen!

Es war eine tolle Woche voller Sonnenschein, Sport und Familienzeit. ☀️ Ich bin so froh, dass wir so viele schöne Momente gemeinsam erleben konnten.

#familienzeit #sommer #ausflug #ostsee #kinder #spaß #sonnenschein #karate