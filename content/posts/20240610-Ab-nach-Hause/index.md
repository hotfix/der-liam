---
title: "Ab Nach Hause"
date: 2024-06-10T22:15:46+02:00
draft: false
---


Heute ist endlich Montag, der Tag, an dem wir endlich mit Mama und Papa nach Hause fahren durften! 

Da einer meiner Blutwerte gestern etwas erhöht war, kam am Morgen nochmal ein Arzt und hat mir Blut abgenommen.  
Diesmal war auch Papa dabei, er hatte mich etwas abgelenkt, als der Arzt mich wieder mit einer Nadel gestochen hat. 

Da meine Hände noch so klein sind, kam beim ersten Mal kein Blut raus, so dass der Arzt mich noch einmal picksen musste.  
Dieses Mal war ich stark und habe auch nicht geweint. Mein Papa und meine Mama waren ganz lieb bei mir. :heart:

Danach musste mich ein Arzt noch untersuchen, denn heute war eine ganz wichtige Untersuchung: die U2. 
Papa sagte, man darf die erst nach 48 Stunden machen, deswegen mussten wir auch bis Montag im Krankenhaus bleiben. 

Die Untersuchung verlief ganz gut, und ich habe sie mit Bravour bestanden! 
Die Prüfungen waren aber gar nicht mal so einfach für einen, der gerade mal 4 Tage alt ist. 

Weil der Arzt mich aber so "gequält" hat mit den ganzen Übungen, habe ich versucht, ihn mit meinem Pipi zu markieren. 
Leider habe ich ihn nicht getroffen, der Glückspilz! 

Danach durften wir endlich einpacken und nach Hause fahren. Als Geschenk haben wir vom Krankenhaus ein tolles Badehandtuch mit der Aufschrift "Ich bin ein Johanniter" bekommen.  

![Ich bin ein Johanniter Badehandtuch](Johannier-Badehandtuch.jpg)

Vielen Dank dafür! 

Mama und Papa haben sich für das Geschenk und die tolle Zeit bedankt.  Wir wurden in den paar Tagen wirklich super versorgt.  
Ich kann wirklich allen Mamas und Papas dieses Krankenhaus empfehlen! 

Danach haben wir unsere Sachen gepackt und sind auch schnell nach Hause gefahren.  Der Kater hat uns schon erwartet.  
Ich weiß aber noch nicht, ob er wirklich froh war, mich zu sehen. Das muss ich mir die nächsten Tage genauer anschauen. 

Obwohl es im Krankenhaus auch gar nicht so schlecht war, bin ich froh, dass wir nun alle Zuhause sind und ab jetzt die Zeit als Familie verbringen werden.  Wir werden sicher viel Spaß zusammen haben! 

**Ich halte euch natürlich auf dem Laufenden!**

**P.S.:** Ein großes Dankeschön an meine Eltern, die sich so toll um mich gekümmert haben! :heart: Und an die Ärzte und Schwestern im Krankenhaus, die so nett zu mir waren. :medical_symbol: :medical_symbol:
