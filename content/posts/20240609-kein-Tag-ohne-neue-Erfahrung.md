---
title: "Kein Tag Ohne Neue Erfahrung"
date: 2024-06-09T21:03:46+02:00
draft: false
---

Guten Abend allerseits, hier meldet sich wieder der kleine Liam! 

Unglaublich, wie schnell die Zeit vergeht – heute ist schon mein dritter Tag auf dieser Welt! 

Die Nacht war ziemlich anstrengend, denn ich schaffe es leider immer noch nicht, richtig zu trinken.  Nach fünf Minuten bin ich schon fix und fertig. 
Mama und Papa versuchen natürlich immer wieder, mich wach zu halten, indem sie meine kleinen Füßchen kitzeln.  Aber der Effekt hält nicht lange an, 
irgendwann höre ich einfach auf zu reagieren und schlafe wieder ein.  Papa meint, ich sei ganz wie er, denn er mag auch gerne schlafen und hat auch kein Problem, schnell einzuschlafen. 

Heute Morgen ist Papa recht früh nach Hause gefahren. Der Kater musste ja wieder gefüttert werden und brauchte mal wieder Aufmerksamkeit.  
Außerdem wollte er schon unser Zuhause etwas vorbereiten, damit wir uns wohlfühlen, wenn wir daheim sind. 

Als er wieder zurückkam, schlief ich allerdings schon, weil ich total erschöpft war.  In der Zwischenzeit ist viel passiert. 
Ich habe wieder Fieber bekommen und der Doktor hat mir Blut abgenommen. Das hat unglaublich wehgetan und ich habe ganz doll geweint.  
Mama war auch sehr traurig, sie konnte nicht zusehen, wie der Doktor mit einer Nadel in meine kleine Hand gestochen hat, um Blut zu entnehmen. 
Und man wusste ja auch nicht, ob ich vielleicht doch eine Infektion habe. 

Nach ein paar Stunden kam dann eine Schwester zu uns ins Zimmer und sagte, dass die Blutwerte soweit ganz gut sind. Ein Wert war zwar etwas erhöht, 
aber man sollte sich da erstmal keine Sorgen machen. Die Blutentnahme hat mich ganz schön fertig gemacht, aber es war eine neue Erfahrung und letztendlich auch halb so schlimm. 

Meine Kacka war heute nicht mehr so schwarz wie gestern, sondern schon deutlich grüner. Das soll wohl ein gutes Zeichen sein, aber ich kann das leider noch nicht beurteilen.  
Papa hat auch gemerkt, dass meine Nabelschnur schon langsam trockener wird. Wahrscheinlich fällt die dann bald ab.

Zum Ende des Tages haben mich meine Eltern gelobt, da ich angefangen habe, deutlich länger an der Brust zu trinken.  Ich glaube, langsam verstehe ich, wie der Hase läuft! 

Jetzt gehe ich erstmal schlafen und mal schauen, was der morgige Tag uns bringt. 

**Bis zum nächsten Mal!** 

**P.S.:** Ein großes Dankeschön an meine Eltern, die sich so toll um mich kümmern! :heart: Und an den Doktor, der trotz der schmerzhaften Blutentnahme so nett zu mir war. :medical_symbol: 
