---
title: "Frühstück mit Freunden! 🥐 Ein schöner Morgen im Café 🌞"
date: 2024-09-14T19:46:25+01:00
draft: false
---

Heute Morgen waren wir mit Luba, Stefan und ihrem Sohn Ben zum Frühstück verabredet. 👨‍👩‍👦 
Das war super cool, die mal wieder zu treffen! Sie planen einen längeren Urlaub und 
wollten uns davor noch einmal sehen. ☀️✈️ Wir haben uns im Café May in Hamburg getroffen, 
wo es ein leckeres Frühstücksbuffet gibt. 🍳🥓

Papa war allerdings nicht so hungrig, weil er gestern auf einem Sommerfest bei seinem Arbeitgeber war. 🌞 
Da wurde wohl viel gegessen, meinte er, also hat nur Mama das Frühstücksbuffet genommen. 
Papa hat sich mehr auf den Kaffee konzentriert. ☕️

Wir haben dann alle gemütlich gegessen und geplaudert. Doch irgendwann wurde mir langweilig, und ich wollte 
raus an die frische Luft! 😴🌬️ Zum Glück war das Wetter noch super, also sind Papa und ich nach draußen gegangen. 
Draußen haben sich dann auch Ben und Stefan zu uns gesellt, und Ben wollte unbedingt 
die Züge an der nahegelegenen S-Bahn-Station anschauen. 🚆 Er hat seinen Papa die ganze Zeit dorthin gezogen! 

Nachdem alle mit dem Frühstück fertig waren, haben wir uns noch eine Weile draußen unterhalten, und dann sind wir alle nach Hause gefahren. 🚗👋


### Herbstspaziergang 🍂

Zu Hause haben wir dann das schöne Herbstwetter genossen und sind noch eine Runde 
spazieren gegangen. 🍁🌞 Frische Luft tut einfach gut – besonders nach so einem spannenden Tag! :baby:👣

Bis bald, ihr Lieben! :wave: :blue_heart:
