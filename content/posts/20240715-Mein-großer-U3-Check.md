---
title: "Mein Großer U3 Check"
date: 2024-07-15T10:32:35+02:00
draft: false
---

Diese Woche war wieder so viel los! Es gab so viele neue Dinge zu entdecken und zu erleben.

Am Anfang der Woche war ich beim Arzt zur großen U3 Untersuchung. Die nette Frau Doktor hat mich ganz genau von Kopf bis Fuß untersucht. 
Ich war so brav und habe sogar gelächelt, als sie mit dem kleinen Hammer auf meine Knie geklopft hat. Sie hat gesagt, ich bin ein großer, starker Junge und mache schon ganz viele tolle Sachen. 
Ich wurde auch wieder gemessen und gewogen. Komisch, dass ich beim Arzt immer kleiner bin als Zuhause. Aber die Frau Doktor hat gesagt, das ist ganz normal.

Bald muss ich noch zu einem speziellen Arzt, der meine Gelenke untersucht. Und dann kommen auch schon die ersten Impfungen. 
Ich bin schon ein bisschen aufgeregt, aber Mama sagt, das ist wichtig, damit ich gesund bleibe. 
Sie hat mir erzählt, dass ich dann vielleicht ein bisschen Fieber bekomme und mich etwas schlapp fühlen werde. Aber sie wird immer bei mir sein und mir helfen.

Papa arbeitet jetzt wieder und verdient Geld für meine Windeln. Wir haben schon ganz viele verschiedene Marken ausprobiert, 
aber Pampers finde ich am besten. Die halten schön trocken und machen keinen roten Po.

Wenn ihr auch [Pampers](https://amzn.to/46mcj0d)&midast; benutzt, dann schaut mal in die Pampers Club App. Mit meinem Code **A798B64** bekommt ihr und ich gleich 200 Punkte geschenkt. 
Das ist super, oder? Mit den Punkten können wir uns dann tolle Rabatte auf Pampers sichern.

Neben den Arztbesuchen habe ich diese Woche auch ganz viel gespielt. Mein neues Rasselspielzeug macht so lustige Geräusche und mein Kuschelhase ist mein bester Freund. 
Ich habe auch viel Zeit mit Mama und Papa verbracht. Wir haben zusammen gelesen, gesungen und sind spazieren gegangen. Besonders schön war es, wenn die Sonne schien.

Ansonsten habe ich viel geschlafen und getrunken. Mama sagt, das brauche ich, um groß und stark zu werden.

Was habt ihr diese Woche so gemacht? Habt ihr auch so viele spannende Abenteuer erlebt?