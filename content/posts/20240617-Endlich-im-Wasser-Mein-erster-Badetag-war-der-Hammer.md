---
title: "Endlich Im Wasser: Mein Erster Badetag War Der Hammer!"
date: 2024-06-17T20:40:20+02:00
draft: false
---

Heute war mein erster Badetag!  Und was soll ich sagen? Es war einfach fantastisch!  Ich habe so das Gefühl, dass ich das Wasser einfach liebe und in Zukunft ein Profischwimmer werde. :swimmer:

Die Hebamme hat mich ganz vorsichtig in die Wanne gelegt und mit lauwarmen Wasser übergossen.  Das Gefühl auf meiner Haut war einfach himmlisch!

Ich hoffe, Mama und Papa haben alles genau verstanden, wie man mich badet.  Es soll ja nicht mein erstes und letztes Mal gewesen sein!

Nach dem Baden wurde ich schön warm in ein Handtuch eingewickelt und Mama hat mir eine Mütze aufgesetzt, damit ich nicht krank werde, da meine Haare noch nass waren.

Leider hatte Mama mein Badetuch, welches ich als [Geschenk im Krankenhaus]({{< relref "20240610-Ab-nach-Hause.md" >}}) bekommen habe, noch nicht gewaschen gehabt.  So musste ich dann mit einem Handtuch von Mama oder Papa vorliebnehmen.

Beim nächsten Mal darf ich aber mein eigenes Badetuch benutzen.  Da bin ich mir ganz sicher!

Das waren alle Neuigkeiten von heute.  Ich gehe jetzt schlafen und melde mich dann wieder, wenn es etwas Neues gibt.

P.S.: Mama, Papa, wann baden wir wieder?  Ich kann es kaum erwarten!