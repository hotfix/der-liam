---
title: "Hello World " 
date: 2024-06-07T20:34:48+02:00
draft: false
---

## Hallo Welt! 

**Es ist soweit!**  Heute ist endlich der Tag, an dem ich das Licht dieser schönen und bunten Welt erblickt habe! 

Nachts hatte Mama auf einmal starke Schmerzen bekommen. Ich konnte es wohl kaum erwarten, endlich die frische Luft zu schnuppern!  Papa hat sofort gewusst, dass es losgeht und hat angefangen, die Minuten zu zählen. ⏱️ Mama hingegen wollte lieber schnell ins Krankenhaus fahren. 

Auf dem Weg dorthin fühlte Mama sich gar nicht gut und hatte starke Wehen.  Papa hat natürlich Gas gegeben, zum Glück war es Nacht und es waren kaum Autos unterwegs. 

Im Krankenhaus angekommen wurden wir von freundlichen Menschen in rosa Kitteln empfangen. Papa meinte, das seien Hebammen, die Mama helfen sollten, mich schnell und gesund zur Welt zu bringen. ‍⚕️‍⚕️

Mehrere Stunden lang war Mama an Geräten angeschlossen, während die Hebammen überwachten, wie es mir im Bauch ging. 🩺 Es hat zwar etwas gedauert, aber ich war geduldig und Mama war tapfer. 

Da Mamas Schmerzen immer stärker wurden und mein Sauerstoffwert plötzlich schwankte, haben die Hebammen einen Arzt :woman: gerufen.  Dieser untersuchte Mama und schlug vor, mich per Kaiserschnitt 	
:scissors: zu holen.

Natürlich waren alle besorgt, dass es mir vielleicht nicht gut gehen könnte. Auch Papa war ganz aufgeregt.  Aber dann ging alles ganz schnell: Mamas Bauch wurde aufgeschnitten und ich endlich auf die Welt geholt! 	
:scissors:

Was für ein Gefühl, endlich da zu sein und Mamas warme Umarmung zu spüren!  Nach der Operation durften wir endlich zu Papa ins Zimmer und als Familie die ersten Momente zusammen genießen. 

Bei der ersten Untersuchung haben die Ärzte festgestellt, dass ich schon ein stolzer kleiner Kerl bin: 54 cm groß und 3435 g schwer! 

Danach durfte ich wieder zu Mama und Papa, und wir wurden in ein gemütliches Zimmer gebracht, wo wir ganz ungestört die erste Zeit zusammen verbringen konnten. 

**Der erste Tag war ganz schön aufregend für mich.** Alles war neu und ungewohnt.  Aber Mama und Papa haben sich liebevoll um mich gekümmert und mir alles gezeigt. :heart:

Ich bin gespannt, was die Zukunft für mich bereithält, und freue mich auf viele schöne Abenteuer mit meiner Familie! 

**Lasst mich euch auf jeden Fall wissen, wie es mir weitergeht!**

**P.S.:** Ein großes Dankeschön an meine tollen Eltern, die mich so liebevoll auf dieser Welt empfangen haben! :heart:
