---
title: "Mama Und Papa Waren Am Ende Ihrer Kräfte Mein Nächtlicher Nervenkrieg"
date: 2024-06-19T21:14:09+02:00
draft: false
---

Heute fange ich mal mit weniger guten Nachrichten an.

Gestern Abend habe ich meinen beiden süßen Eltern ordentlich Stress bereitet. Ich konnte einfach nicht einschlafen und habe ständig geweint.  
Mama war schon ratlos und wollte schon fast weinen, weil sie einfach nicht mehr wusste, was sie tun sollte.  
Sie hat mich mehrmals versucht zu stillen, aber das hat leider keinen Erfolg mehr gebracht. Irgendwann war auch nicht mehr genug Milch da.

Papa ist dann kurz vor 22 Uhr schnell zur Rewe bei uns im Dorf gefahren und hat noch [Hipp Babynahrung](https://amzn.to/45EcXWF)&midast; gekauft. Zum Glück hatte der Laden noch offen.

Zuhause angekommen, hat er schnell noch ein Fläschchen fertig gemacht, welches ich dann getrunken habe.
Danach musste auch noch meine Windel gewechselt werden, da die schon voll war.  Mit dem sauberen Popo wollte Mama mich ins Bett bringen, aber ich wollte einfach nicht einschlafen.
So ging das glaube ich bis Mitternacht.  Irgendwann habe ich dann noch ein Fläschchen von dem Zeug bekommen, was Papa gekauft hatte, und dann haben die beiden es endlich geschafft.  
Ich schlief ein. Die beiden sind dann hoffentlich auch gleich ins Bett und sind auch schnell eingeschlafen.

Die Nacht ging dann aber ganz okay.  Ich schlief meine drei oder vier Stunden, bevor ich wieder Hunger bekam.
Puh, da habe ich meine beiden lieben Eltern echt herausgefordert.

Tagsüber habe ich dann wieder relativ gut geschlafen. Papa hat sogar in der Zeit geschafft, unseren Rasen zu mähen und im Garten etwas Ordnung zu schaffen.

Ansonsten war der Tag nicht so spannend.
Mama sagte noch, dass wir morgen einen wichtigen Termin haben.  Sie macht sich schon ein bisschen Sorgen, ob es dann klappt, dass ich in der Zeit schlafe.
Ich freue mich trotzdem auf den morgigen Tag.