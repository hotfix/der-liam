---
title: "Piks, Piks und ein langer Tag! 🩹 Mein Impf-Abenteuer beim Doktor 👩‍⚕️👨‍⚕️"
date: 2024-09-10T21:59:27+01:00
draft: false
---

Heute stand wieder eine Impfung an! 💉 Obwohl wir pünktlich beim Doktor waren, 
mussten wir noch eine halbe Stunde im Wartezimmer warten. :hourglass_flowing_sand: 
Diesmal hat Papa entschieden, mich mit dem Kinderwagen zum Arzt zu fahren. :baby: 
Doch der musste draußen im Flur bleiben – also hat Papa mich die ganze Zeit auf dem Arm getragen. :muscle: 
Das fand er ganz schön anstrengend, denn ich werde ja langsam schwer! Papa meint, 
ich wiege bestimmt schon 10 kg… aber nee, so schwer bin ich noch nicht. 😉

Als wir dann endlich dran waren, ging alles ganz schnell. Ich habe mal wieder zwei Pieks in 
die Oberschenkel bekommen. Aua! 😢 Danach durften wir schon wieder gehen. Kaum waren wir aus dem Gebäude raus, 
bin ich auch schon eingeschlafen. :zzz: Als wir zu Hause ankamen, war ich kurz wach und dann wieder im Bettchen eingeschlummert. :sleeping:

Aber jetzt habe ich wieder etwas Fieber und kann nicht gut schlafen. :face_with_thermometer:
Papa hat mir ein Zäpfchen gegeben – ich hoffe, es wirkt bald, damit ich endlich zur Ruhe komme.

Ich wünsche euch schon mal eine gute und erholsame Nacht! :night_with_stars: Bleibt gesund und munter! :blush:🌙

---

Bis bald, ihr Lieben! :wave: :blue_heart:
