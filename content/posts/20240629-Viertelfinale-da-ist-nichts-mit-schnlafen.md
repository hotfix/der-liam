---
title: "Achtelfinale - Da Ist Nichts Mit Schlafen Angesagt"
date: 2024-06-30T10:00:44+02:00
draft: false
---

Oh nein! Gestern konnte ich wieder mal tagsüber nicht einschlafen. So dass, wir die ganze Zeit zuhause waren.
Mama und Papa sagen, dass  es kein Sinn macht mit mir in dem Zustand spazieren zu gehen, da ich dann immer anfange zu weinen, wenn wir unterwegs sind. 
So blieb Papa nur übrig, alleine zum Einkaufen zu fahren.

Währenddessen hat Mama es mit weißem Rauschen geschafft, mich endlich ins Bett zu bringen. Aber nicht für lange! Nach kurzer Zeit war ich dann wieder wach!

Gestern Abend war ein ganz besonderer Tag: Deutschland spielte gegen Dänemark bei der EM 2024! Mama und Papa wollten mich dann pünktlich vorm Spiel ins Bett bringen, aber ich hatte ganz andere Pläne! 
Ich wollte unbedingt dabei sein, also tat ich alles, um nicht einzuschlafen.
Leider hat es nicht geklappt. Ich war so müde, dass ich am Ende doch eingeschlafen bin.

Aber zum Glück hatte Papa eine tolle Überraschung für mich! Als ich mitten in der Nacht zum Windeln wechseln aufwachte, flüsterte er mir voller Freude zu: "Deutschland hat 2:0 gewonnen!"

Ich war so glücklich! Zwar hatte ich das Spiel verpasst, aber ich freute mich riesig über den Sieg unserer Mannschaft.

Vielleicht darf ich ja beim nächsten Spiel im Viertelfinale dabei sein und Deutschland mit meinen Baby-Jubelrufen unterstützen.
Und denkt daran: Auch die größten Schlafmützen können zu echten Fußballhelden werden! ⚽️