---
title: "U4 Und Meine Erste Impfung Und Natürlich Grillfest mit Gästen"
date: 2024-08-11T16:25:29+02:00
draft: false
---

Huiii, die Zeit fliegt nur so vorbei! :dash: Wieder ist eine Woche um, und es war echt anstrengend. 
Am Donnerstag hatte ich meine U4-Untersuchung und habe die ersten Impfungen bekommen. :syringe: Das war echt heftig!

### Untersuchung und Impfung :thermometer_face: 

Die Untersuchung selbst war eigentlich ganz entspannt. :relieved: Der Doktor hat gesagt, dass ich gut wachse und im Durchschnitt liege. Alles im grünen Bereich! :chart_with_upwards_trend:

Die erste Impfung war auch noch ganz okay. Da gab's nämlich was zum Trinken. :baby_bottle: Hat aber nicht so dolle geschmeckt... :grimacing: 
Doch dann kam der Horror! Ich wurde in beide Oberschenkel gepiekst. :scream: Das tat soooo weh, und ich hab geheult wie eine Sirene! :loud_sound: 
Papa hat mich auf den Arm genommen und versucht, mich zu beruhigen. Das hat echt gedauert... :cry:

Abends ging's mir dann nicht so gut, und ich hatte ein bisschen Fieber. :thermometer: Papa hat mir dann ein Zäpfchen gegeben. :pill: 
Nach einiger Zeit ging's mir zum Glück wieder besser, und ich konnte dann auch einschlafen. :zzz:

### Grillfest und Besuch :grill:

Am Freitag ist Papa abends einkaufen gefahren. :shopping_cart: Der hat so viel Grillzeug für Samstag eingekauft, das wollen Vegetarier gar nicht wissen! :cut_of_meat: 
Samstag früh ist Papa noch schnell zum Friseur gefahren, es war echt mal wieder Zeit, Ordnung auf dem Kopf zu schaffen. :haircut_man:

Nachmittags kamen dann die Gäste! :family_man_woman_girl: Ich hab mich sooo gefreut, Oma, Opa und Onkel Pawel zu sehen! :older_woman: :older_man: :man: 
Wir hatten eine schöne Zeit zusammen, schön gegrillt und dann auch zusammen gespielt. :video_game: Ich hab die ganze Zeit, bis sie weg waren, nicht geschlafen. 
War echt anstrengend, aber auch total toll! :smiling_face_with_three_hearts:

### Meine neuen Tricks :baby_tone1:

Diese Woche hab ich auch einige neue Sachen gelernt! :nerd_face: Ich folge jetzt mit meinen Augen den Gegenständen, die mir gezeigt werden. :eyes: 
Und ich lächle noch viel mehr! :grinning: Außerdem quicke ich jetzt recht laut, fast schon wie ein kleines Meerschweinchen! :pig:

Ich bin mittlerweile auch deutlich aktiver geworden und sooo neugierig! :exploding_head: Beim Spazierengehen mag ich es total, die Bäume und den Himmel zu beobachten. :deciduous_tree: :cloud: 
Ich sehe, wie wunderschön farbig diese Welt ist! :rainbow:

Bis bald, ihr Lieben! :wave: :blue_heart:
