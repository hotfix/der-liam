---
title: "Mein Erstes Kacka Attentat Auf Die Hebamme"
date: 2024-06-11T23:40:00+02:00
draft: false
---

Heute war die Hebamme bei uns zu Besuch, um sich nach mir und Mama zu erkundigen.

Sie hat mich gründlich untersucht, meinen Bauchnabel desinfiziert und meine Windel ausgezogen, um mich sauber zu machen.  Ich war schon ganz gespannt, was sie als nächstes machen würde.

Plötzlich kam mir eine Idee: Ich hatte ein kleines "Begrüßungsgeschenk" für die Hebamme vorbereitet!  Kaum war mein Popo picobello sauber und sie nichts ahnend, 
habe ich meine "Kanone" geladen und mit einem gezielten Schuss die Hebamme mit meiner Kacka vollgeschossen!

Mama und Papa fanden das total witzig und lachten lauthals.  Die Hebamme konnte sich ein Lachen auch nicht verkneifen, aber wahrscheinlich war es für sie nicht ganz so lustig wie für meine Eltern.  
Aber was soll's, das gehört halt zu ihrem Job dazu!

Danach hat sie Mama und Papa noch einige verschiedene Tragetechniken für Babys gezeigt, unter anderem den Fliegergriff.  Ich finde es wirklich toll, wenn Mama oder Papa mich so tragen. 
Das beruhigt mich und ich schlafe dann auch ganz schnell ein.

Ansonsten war der Tag heute gar nicht so spannend.  Ich habe viel geschlafen und getrunken.

Morgen kommt die Hebamme wieder vorbei.  Ich bin schon ganz gespannt, ob sie dann auf meinen Angriff vorbereitet sein wird!

Fast hätte ich es vergessen: Heute habe ich meine erste Post bekommen!  Und nein, das war nicht die Post vom Finanzamt.  
Papa meinte scherzhaft, dass ich als erstes immer eine Steuer-ID zugeschickt bekommen würde, damit ich gleich fleißig Steuern zahlen kann.
Tatsächlich war es aber ein kleines Paket von Papas Arbeitskollegen Stefan.  Er hat mir ein tolles Geschenk zukommen lassen: ein Body und ein paar Söckchen mit St. Pauli Logo.  

![Body- Socken Set von St.Pauli](st.pauli-body-und-socken.jpg)

Papa erzählte mir, dass es ein Fußballverein aus Hamburg ist.  Ob ich wohl auch mal Fußball spielen werde, wenn ich groß bin?

Zusätzlich war ein kleines Spielzeug mit Rasseln dabei und eine kleine Packung Schokolade für Mama und Papa, damit sie sich stärken können.  Vielen Dank, lieber Stefan, für das tolle Geschenk!

P.S.: Ein großes Dankeschön an die Hebamme für ihren Besuch und  die wertvollen Tipps! Ebenso ein riesen Dankeschön an Stefan für die tollen Geschenke!