---
title: "Guuude!✨👶 Lange nix gehört, wa?"
date: 2024-08-04T14:15:09+02:00
draft: false
---

Huch, hab mich ja echt lange nicht gemeldet! :pleading_face: Zwei Wochen sind schon vorbei, seitdem ich das letzte Mal gequiekt hab. Aber hier bin ich wieder, frisch und munter! :muscle: :baby_bottle:

### Was war los die Woche?

Ohhh, war wieder richtig viel los! :hatched_chick: Am Dienstag hatte ich einen Termin beim Orthopäden, der hat meine Hüften angeschaut. :eyes: Der Doktor hat gesagt, dass alles tipi topi ist! :tada: Mami und Papi waren soooo erleichtert und ich natürlich auch. Fühl mich super mit meiner Entwicklung! :blush:

Am Freitag war Papa unterwegs, er meinte, er muss das Auto untersuchen lassen. :red_car: :wrench: Nach der Arbeit war er dann für ein paar Stunden weg. Hab ihn aber trotzdem lieb vermisst! :pleading_face: :heart:

Samstag war Opa's Geburtstag! :birthday: Aber wir haben ihn nicht besucht, weil es soooo warm war. :sunny: Stattdessen sind wir mit Mama und Papa zur Ostsee gefahren! :ocean: Und dieses Mal ist Mama auch ins Wasser gehüpft! :swimmer: War voll lustig, aber das Schlafen hat an dem Tag nicht so gut geklappt. Ich glaube, es war einfach zu warm! :hot_face: :zzz:


### Meine Entwicklung! :rocket:

In der Entwicklung mache ich auch Fortschritte! Ich fange langsam an, mein Köpfchen zu halten, wenn ich auf dem Bauch liege. Zwar noch nicht so lange, aber hey, es wird langsam! :hatching_chick: Ab und zu stecke ich mir auch meine Faust in den Mund. Schmeckt zwar komisch, aber irgendwie auch gut? :thinking: :fist: :yum:

Mit dem Schlaf wird es langsam besser... glaub ich zumindest! :sleeping: Und ich lächle jetzt viiiel mehr! :smile:

Bin schon gespannt, was als nächstes kommt. Für nächste Woche haben wir Opa, Oma und Onkel Pawel eingeladen. Da wollen wir Schaschlik machen. :meat_on_bone: Ich hoffe, Papa schafft es noch, alle Lebensmittel zu besorgen. Drückt die Daumen! :crossed_fingers:

Bis bald, ihr Lieben! :wave: :blue_heart:
