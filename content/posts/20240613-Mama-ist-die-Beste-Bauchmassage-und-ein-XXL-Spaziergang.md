---
title: "Mama Ist Die Beste! Bauchmassage Und Ein XXL-Spaziergang"
date: 2024-06-13T20:47:28+02:00
draft: false
---

Heute Morgen ist Mama ganz früh aufgestanden. Sie konnte nicht mehr schlafen, nachdem sie und Papa mich gegen 6 Uhr wieder ins Bett gebracht hatten.  
Papa hingegen brauchte noch ein Stündchen und schlief sofort wieder ein.

In der Zeit, in der wir mit Papa noch schliefen, hat Mama unser Zuhause aufgeräumt und sauber gemacht. Vielen Dank, Mama, dass du so fleißig bist und dafür sorgst, dass wir uns alle Zuhause wohlfühlen!  Du bist die Beste! :heart:

Mamas Hebamme, die uns heute wieder besucht hat, zeigte meinen Eltern heute, wie sie mir eine Bauchmassage machen können.  
Diese fand ich super schön und habe die paar Minuten richtig genossen.  Ich hoffe, dass ich diese jetzt auch täglich bekomme!

Übrigens nehme ich weiterhin fleißig an Gewicht zu.  Nur noch 15 Gramm und ich habe mein Geburtsgewicht erreicht.  Ich denke, morgen knacke ich den Wert!

Nachmittags sind wir dann eine große Runde in meinem Kinderwagen spazieren gefahren.  Zwei Stunden an der frischen Luft haben mir echt gut getan, und ich konnte ganz lange gut schlafen.

Ich freue mich schon auf morgen, denn morgen ist mein erster Mini-Geburtstag!  Ich werde schon eine ganze Woche alt sein!

Bis zum nächsten Mal,

Euer Liam!