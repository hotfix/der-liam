---
title: "Geburtsurkunde Besorgt - Autofahrt War Ganz Schön Lässig!"
date: 2024-06-20T21:50:30+02:00
draft: false
---

Der Termin, den ich gestern erwähnt hatte, war heute im Standesamt wegen meiner Geburtsurkunde.

Warum auch immer wurde nur meine Mama eingeladen. Sie konnte mich aber nicht alleine mit Papa zuhause lassen. Ich könnte ja in der Zwischenzeit Hunger bekommen, dann wäre Papa aufgeschmissen.

Deswegen sind wir zu dritt gefahren. Das war quasi meine erste Autoreise (die Heimreise aus dem Krankenhaus zähle ich jetzt mal nicht dazu).

Ich muss sagen, es war schön.  Papa hat mir einen ganz coolen und bequemen Autositz gekauft.  Wenn ich mich richtig erinnere, nannte er ihn ["Maxi Cosi Pebble"](https://amzn.to/3VEhzYa)&midast; in einem schönen Grün.

Das Standesamt liegt auf der anderen Seite der Elbe in der Stadt Geesthacht. Ich habe die ganze Zeit, als wir unterwegs waren, schön geschlafen. 
Zuhause haben Mama und Papa mir erzählt, dass ich jetzt sogar drei Geburtsurkunden habe. Obwohl die alle gleich aussehen, sind sie für unterschiedliche Behörden bestimmt.
Ich habe aber nicht verstanden, warum man drei braucht, wenn die eh alle gleich sind.  Man kann ja auch nur eine haben und diese dann als Kopie den Behörden zur Verfügung stellen?  
Langsam merke ich, dass das Leben auf dieser Welt ganz schön kompliziert ist und man vieles auch nicht gleich versteht.

Morgen ist dann wieder die Hebamme bei uns zu Besuch.  Bin gespannt, was es dann Neues gibt.

Bis dahin, euer Liam

P.S.: Mama und Papa, wann fahren wir wieder Auto?  Ich fand das ganz toll!