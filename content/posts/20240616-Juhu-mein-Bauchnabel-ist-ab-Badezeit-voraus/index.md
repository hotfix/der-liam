---
title: " Juhu Mein Bauchnabel Ist Ab Badezeit Voraus"
date: 2024-06-16T18:42:38+02:00
draft: false
---

Heute ist ein großer Tag für mich: Meine Bauchnabelschnur ist abgefallen!  

![meine Bauchnabelschnur](Bauchnabelschnur.jpg)

Das bedeutet, dass ich bald endlich baden darf.

Mama und Papa trauen sich allerdings noch nicht ganz, mich heute schon in die Wanne zu hauen. Sie wollen lieber auf die Hebamme warten, die morgen vorbeikommt.
Ich kann aber schon gar nicht mehr lange warten! Ich will unbedingt ein Bad nehmen!

Mein Popo ist zwar immer noch etwas rot, aber es wird langsam besser. Zumindest glaube ich das.

Das Wetter wird auch endlich besser und ist nicht mehr so regnerisch. :partly_sunny: Papa wollte schon unseren Rasen mähen, da er ziemlich gewachsen ist.  
Er hat aber wohl vergessen, dass es heute Sonntag ist und man am Sonntag keinen Lärm machen darf.  Zum Glück hat Mama ihn daran erinnert, sonst hätte er bestimmt Ärger bekommen.

Unsere Restmülltonne :wastebasket:  wird langsam voll. Ich bin halt fleißig mit Windeln vollmachen.  Papa hat zwar schon eine größere Tonne bestellt, aber wer weiß, wann die kommt. 
Er meinte, wir sollten noch irgendwie bis Donnerstag durchhalten, da wird dann der Müll abgeholt.

So, für heute war's das erstmal von mir. Bis morgen!

Ich freue mich schon riesig auf meinen Wellnesstag morgen! 

P.S.: Mama und Papa, bitte beeilt euch mit dem Baden! :bath: