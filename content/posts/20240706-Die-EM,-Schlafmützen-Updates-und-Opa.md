---
title: "Die EM ⚽, Schlafmützen Updates Und Opa"
date: 2024-07-06T21:05:23+02:00
draft: false
---

Oh je, die Enttäuschung war groß! Gestern fand das Viertelfinale der EM statt, Deutschland gegen Spanien. 
Wie gerne hätte ich das Spiel gesehen, aber leider war ich so müde, dass ich die ganze Zeit durchgeschlafen habe.

Mama meinte sogar, dass es an meinem Schlafen lag, dass Deutschland verloren hat. Bei den anderen 
Spielen habe ich nämlich nicht geschlafen und meine lieben Eltern ganz schön auf Trab gehalten.
Aber Papa hat mich getröstet und gesagt, dass das Spiel trotzdem gut war.

Ich glaube, diese Woche hatte ich meinen ersten Entwicklungssprung. Zumindest war mein Verhalten ziemlich durcheinander. 
Mama und Papa waren mal wieder ratlos. Papa meinte sogar, dass ich ein Software-Update bekommen hätte. 
Jetzt sind sie gespannt, was ich so alles Neues kann.

Heute waren wir Oma und Opa besuchen. Endlich habe ich meinen Opa kennengelernt! 
Er konnte gar nicht glauben, wie klein ich noch bin. Opa, ich bin doch erst einen Monat alt (werde ich morgen), da konnte ich noch nicht so schnell wachsen!
Es war super toll bei Oma und Opa. Sie sind richtig cool, und ich freue mich schon darauf, wenn ich mit ihnen richtig spielen kann.

Morgen werde ich schon einen Monat alt! Hurra! Ich werde euch berichten, wenn es wieder etwas Neues gibt.

P.S.: Vergesst nicht, mir Glückwünsche zu meinem Geburtstag zu schicken!