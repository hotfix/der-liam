---
title: "Mein zweiter Tag auf der Welt!"
date: 2024-06-08T20:05:33+02:00
draft: false
---

Heute ist schon der 8. Juli und ich kann euch berichten, dass meine erste Nacht mit Mama und Papa einfach toll war!  
Wir haben ganz viel "Bonding" gemacht, was bedeutet, dass Mama und ich ganz kuschelig zusammen im Bett lagen und uns gegenseitig Wärme und Geborgenheit gegeben haben. 
Papa durfte natürlich auch mitkuscheln! 

Zum Stillen hat Mama mir ihre Brust gegeben, aber ich bin leider noch ein bisschen unerfahren und konnte nicht so gut trinken.  
Die Schwestern haben deshalb mein Fieber gemessen, weil sie vermuteten, dass ich deswegen so wenig Appetit habe. Tatsächlich war meine Temperatur etwas erhöht, so um die 37,8 Grad. 	
:thermometer:

Zum Glück konnte die Temperatur durch zusätzliche Flüssigkeit gesenkt werden. Da ich noch nicht so viel trinke, bekam ich neben Mamas Milch zusätzlich noch etwas Nahrung aus der Flasche. 
So wollte man sichergehen, dass ich genug Flüssigkeit zu mir nehme. 

Weil es bei meiner Geburt ja einige Komplikationen gab, wurde ich nach der Entbindung an verschiedene Geräte angeschlossen. 
So konnten die Schwestern meinen Puls und Sauerstoffgehalt überwachen und sicherstellen, dass alles in Ordnung ist. :stethoscope:

Die Schwestern waren super nett und haben Mama und Papa auch gezeigt, wie sie mich richtig wickeln und versorgen können. Das ist ja gar nicht so einfach, wenn man so klein und unerfahren ist wie ich! 

Als meine erste Windel voll war, haben die Schwestern erklärt, dass es sich dabei um sogenanntes Kindspech handelt, was ganz normal ist. 
Papa durfte dann sogar seine erste Windel an mir wechseln. Der war zwar etwas ungeschickt, aber ich fand es süß, dass er sich so bemüht hat. 

Im Laufe des Tages habe ich noch ein paar weitere Windeln voll gemacht, so dass Papa fleißig üben konnte.  Mama war nach dem Kaiserschnitt leider noch sehr erschöpft 
und konnte nicht so viel aus dem Bett. Sie hat aber immer geduldig auf mich gewartet, wenn ich Hunger hatte oder gewickelt werden musste. :heart:

Am Abend kam Papa dann wieder vom Zuhause zurück, wo er sich um unseren Kater kümmern musste.  Er hat mir aber versprochen, dass er ganz schnell wieder zu mir kommt, 
und so war ich auch gar nicht traurig, als er kurz weg war. 

Als Papa wieder da war, haben wir noch ein bisschen zusammen gekuschelt und gespielt. Es war so schön, endlich mit meiner ganzen Familie vereint zu sein! 

Weil heute etwas Fieber hatte, kam nochmal ein Doktor vorbei, um mich zu untersuchen.  Er hat mich gründlich untersucht, aber zum Glück nichts festgestellt. 
Er meinte, dass es wahrscheinlich nur an der Umstellung auf die neue Umgebung liegt und dass mein Fieber morgen wieder verschwunden sein sollte.

Trotz des kleinen Doktorbesuchs war der zweite Tag auf der Welt voller neuer Erfahrungen und Abenteuer. Ich bin gespannt, was die nächsten Tage noch bringen!

**Bis zum nächsten Mal!** 

**P.S.:** Ein großes Dankeschön an meine Eltern, die netten Schwestern im Krankenhaus und den Doktor, der sich so gut um mich gekümmert hat! :heart:
