---
title: "Papa Im Visier: Pipi Attacken Und Windel Chaos"
date: 2024-06-12T22:11:34+02:00
draft: false
---

Guten Abend zusammen!

Die Nächte laufen bis jetzt ganz gut. Ich wache nicht so oft auf und lasse meine lieben Eltern endlich ein bisschen ausschlafen.

Ab und zu ärgere ich meinen Papa aber gerne mal, indem ich ihn beim Windeln wechseln anpische oder wenn er mir eine frische Windel anzieht, diese dann gleich wieder vollmache. 
Ich glaube, er ist nicht ganz begeistert von solchen Aktionen, aber ich teste trotzdem gerne seine Grenzen aus. Bis jetzt ist er aber immer ganz lieb zu mir geblieben. :heart:

Ansonsten war heute ein weiterer aufregender Tag für mich. Gegen Mittag kam die Hebamme mal wieder vorbei, um mich zu untersuchen, zu wiegen und zu wickeln. :stethoscope: :balance_scale: 
Ich nehme übrigens gut zu und habe schon fast mein Geburtsgewicht wieder erreicht.

Mama und Papa erkundigten sich bei der Hebamme, wie sie mir am besten die Tabletten mit Vitamin D und Fluorid geben können.  Diese soll ich nämlich ab meinem 7. Lebenstag bekommen.

Nachdem die Hebamme weg war und sich das Wetter endlich etwas gebessert hatte (die letzten Tage war es ziemlich kühl und regnerisch), beschlossen wir zu dritt, einen kleinen Spaziergang zu machen. :sun_with_face: 

Für mich war natürlich klar, dass ich die ganze Zeit während des Ausflugs schlafen würde, aber ich wollte unbedingt meinen neuen Kinderwagen probefahren.  
Dieser ist übrigens ein Geschenk von meinem Onkel Pawel, der mit seiner Familie in Hamburg wohnt. 

Also mixte mir Mama schnell einen leckeren Milkshake und dann ging es auch schon los.  Papa zog mich warm an, damit mir draußen nicht kalt wird, und dann fuhren wir los.

Mama und Papa entschieden sich für eine kleine Runde, was mir ganz recht war. Nach dem Milkshake war ich nämlich schon so müde, dass ich schnell einschlief.

Später erzählte mir Mama, dass wir in einem Geschäft waren und einige Sachen eingekauft hatten, und dann auch schon wieder zurück gefahren sind.

Mir hat die Probefahrt auf jeden Fall gefallen und ich hoffe, dass wir bald wieder rausfahren werden.

Nachdem wir wieder Zuhause waren, schlief ich noch eine ganze Weile. In dieser Zeit telefonierte Papa mit meiner Oma und zeigte ihr meine Wenigkeit.  
Leider habe ich sie bis heute noch nicht kennengelernt, aber ich hoffe, dass ich das bald tun werde.

Papa sagt, sie hat mich ganz doll lieb und möchte mich gerne in ihre Arme schließen. Das wünsche ich mir auch sehr! :heart:

Ich hoffe, das waren dann alle Ereignisse von diesem Tag. Jetzt gehe ich schlafen und wünsche allen eine gute und ruhige Nacht!

Bis zum nächsten Mal!

Euer Liam