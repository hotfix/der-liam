---
title: "Hebamme war da, Onkel per Video kennengelernt und viel frische Luft ein ereignisreicher Tag"
date: 2024-06-21T22:50:25+02:00
draft: false
---

Heute Mittag kam wieder die Hebamme vorbei.
Sie hat gesagt, dass man jetzt langsam auch anfangen kann, meinen ganzen Körper zu massieren, und hat Mama und Papa auch ein paar Techniken gezeigt.
Danach hat sie mich gewogen.  3.760 g!  Ich nehme gut zu.  Bald kann Papa mich nicht mehr so lange auf den Armen tragen.

Sie hat auch meinen Kopf gemessen - 38 cm.  Sie meinte, dass er eigentlich ziemlich schnell gewachsen ist.  Bei der Geburt war er angeblich 35 cm.  Man sollte es im Blick behalten.
Mama und Papa machen sich jetzt auch ein bisschen Sorgen deswegen.

Nachmittags sind wir dann spazieren gegangen, und ich konnte mal wieder nicht schlafen.
Als wir noch recht weit von Zuhause entfernt waren, habe ich angefangen zu weinen.  Papa hat versucht, mich mit Babymusik zu beruhigen, aber das hat leider keinen Erfolg gebracht.
Dann nahm er mich auf die Arme und wir sind so Richtung Zuhause gegangen.  Nach einigen Metern bin ich wohl in seinen Armen eingeschlafen.
Er legte mich wieder in den Kinderwagen und wir haben dann noch einen Umweg auf dem Weg nach Hause gemacht, da ich so schön schlief.
Kurz bevor wir Zuhause ankamen, wurde ich wieder wach.  Ich glaube, ich hatte schon wieder Hunger.  Die frische Luft tat mir halt gut.

Vor 10 Minuten habe ich meinen Onkel Nikolai, den Bruder von meiner Mama, kennengelernt.  Er hat uns per WhatsApp angerufen.
Der Onkel lebt in Russland und kann uns leider noch nicht besuchen kommen.
Deswegen haben wir uns erstmal per Video unterhalten.
Ich würde mich super freuen, wenn er uns besuchen kommt. Dann können wir uns noch besser kennenlernen und die Zeit zusammen verbringen.

Jetzt bin ich erstmal müde und gehe ins Bett.

Gute Nacht euch allen!

P.S.: Onkel Nikolai, wann kommst du uns besuchen?  Ich warte schon ganz sehnsüchtig auf dich!