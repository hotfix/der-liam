---
title: "Wasserrauschen Und Omas Knuddeln: Endlich Mal Wieder Gut Geschlafen!"
date: 2024-06-22T19:53:35+02:00
draft: false
---

Gestern konnte ich mal wieder nicht einschlafen. Mama hat im Internet gelesen, dass es bestimmte Geräusche gibt, die Babys beruhigen sollen. 
Sie ist ins Bad gegangen und hat dann den Wasserhahn aufgedreht.  Das Geräusch des fließenden Wassers hat glaube ich eine beruhigende Wirkung auf mich gezeigt und ich habe langsam angefangen, mich zu beruhigen.

Papa hat dann bei Spotify Podcasts mit unterschiedlichen Hintergrundgeräuschen gefunden.

Mit dem Hintergrundgeräusch konnte ich dann, ich glaube, so ca. nach einer bis eineinhalb Stunden, mich beruhigen und einschlafen.  
Ich konnte dann auch wirklich gut schlafen und fast 4 Stunden durchschlafen.  Das hat meine Eltern dann sehr gefreut.

Vormittags bekamen wir dann Besuch.  Mein Onkel Pawel und meine Oma sind vorbeigekommen.

Sie haben uns so viele Sachen und Geschenke mitgebracht.  Das war so cool!

Meine Oma war so froh, mich zu sehen, dass sie mich nicht aus den Armen geben wollte.  Gemeinsam haben wir dann noch ein paar Fotos gemacht.  
Danach hat Oma mit mir noch etwas gespielt und dann mussten die wieder mit Onkel los, weil Onkel seine Tochter aus der Schule abholen musste.

Es war eine tolle Zeit mit den beiden.  Ich hoffe, ich kann sie bald wiedersehen.

Schade, dass Opa nicht mitgekommen ist.  Ich möchte ihn auch schon so gern kennenlernen.

Aber ich denke, bald werden wir Oma und Opa besuchen kommen.  Dann kann ich richtig Zeit mit ihnen verbringen.

Als die beiden weg waren, habe ich noch ein bisschen was gegessen und dann sind wir spazieren gefahren.

Ich liebe es, an der frischen Luft die Zeit zu verbringen.

Für heute war's das mit Neuigkeiten.  Ich hoffe, ich mache heute Abend meinen Eltern keine Sorgen, wenn sie mich ins Bett bringen.