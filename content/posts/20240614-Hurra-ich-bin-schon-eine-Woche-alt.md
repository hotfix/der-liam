---
title: "Hurra, Ich Bin Schon Eine Woche Alt!"
date: 2024-06-14T23:23:02+02:00
draft: false
---

Hurra, heute ist mein Tag! Ich bin genau eine Woche alt. Was für ein Datum, oder?

Mittags waren wir mit Mama und Papa wieder spazieren. Dieses Mal waren wir am Deich. Es war wunderbar. Die frische Luft tat mir gut und ich konnte schön schlafen. :sleeping:

Heute war die Hebamme zum letzten Mal in dieser Woche bei uns. Ich habe tatsächlich die verlorenen Kilos wieder drauf! Das finde ich gar nicht mal so schlecht, oder was meint ihr so?

Abends war das EM 2024 Eröffnungsspiel: Deutschland gegen Schottland. :soccer: :de:

Papa und Mama wollten es sich in Ruhe anschauen, da ich geschlafen habe. So gegen Halbzeit bin ich aber aufgewacht und habe den beiden alles "versaut".

Mama und Papa waren aber zum Glück nicht böse auf mich.  Deutschland führte zu dieser Zeit bereits 3:1, es konnte also nichts mehr schiefgehen. :soccer: :de: :scotland: 

Deswegen sind die beiden zu mir gekommen und haben ein bisschen Zeit mit mir verbracht, bevor ich dann eine neue Windel bekommen habe und wieder ins Bett ging. :bed:

Alles in allem war es ein toller Tag für mich.  Ich bin gespannt, was die nächste Woche bringt!

P.S.: Ein großes Dankeschön an die Hebamme für ihren Besuch und ihre Tipps! Du bist die Beste!

P.P.S.: Und natürlich auch ein großes Dankeschön an Mama und Papa für die tolle Woche!  Ihr seid die besten Eltern der Welt! :heart:
