---
title: "Geschenke aus Übersee! 🎁 Ein Überraschungspaket aus Kanada! 🍁"
date: 2024-08-13T19:48:19+02:00
draft: false
---

Letzte Woche hab ich ja ganz vergessen zu erwähnen, dass ich noch eine Menge Geschenke bekommen habe! :gift: Unsere Verwandten aus 
Kanada :canada:🍁 – ich wusste ja gar nicht, dass wir dort welche haben!  – haben mir eine ganze Ladung Geschenke geschickt. :package:

### Meine neuen Schätze :star2:

Folgende Sachen waren unteranderem dabei:

- [Ein Babyfläschchen-Sterilisator von Philips](https://amzn.to/46WVSYL)&midast; :baby_bottle:
- [Ein Babyphone von Philips](https://amzn.to/3SWU2Bg)&midast; :pager:
- [Ein Fruchtsauger Set](https://amzn.to/4dvlka1)&midast; :strawberry:
- [Ein Baby Stoff-Knisterbuch](https://amzn.to/46TRidO)&midast; :books:
- [Rasselsocken](https://amzn.to/4cCQcV5)&midast;  :socks:
- [Schlummer Otter](https://amzn.to/3SYRdzJ)&midast;  :otter:

Und noch einige tolle Kleinigkeiten dazu! :tada:

Ich habe mich riiiesig gefreut! :blush: Aktuell zeigen mir Mami und Papi schon regelmäßig das Knisterbuch. Es ist sooo schön farbig und es sind viele tolle Tiere abgebildet. :lion: :elephant: :monkey:

Den Babyfläschchen-Sterilisator haben wir bis jetzt noch nicht benutzt, weil ich ja eigentlich noch gut mit der Milchbar bedient werde. :milk_glass:

Ich freue mich schon riesig darauf, alle anderen Sachen auszuprobieren! :star_struck:

Und ich würde mich sehr freuen, unsere Verwandten aus Kanada 🍁 bei uns begrüßen zu dürfen! Vielleicht kommen wir ja auch irgendwann, um sie in Kanada zu besuchen. :airplane:

Bis bald, ihr Lieben! :wave: :blue_heart:


P.S. &midast; Übrigens wenn ihr über meine Links  etwas kauft, verdient Papa eine kleine Provision von Amazon. Das Taschengeld werden Papa und Mama nutzen, um mir neues Spielzeug und Windeln zu kaufen.

