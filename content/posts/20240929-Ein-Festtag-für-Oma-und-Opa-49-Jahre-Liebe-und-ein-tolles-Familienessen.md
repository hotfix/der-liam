---
title: "Ein Festtag für Oma und Opa 🎉 49 Jahre Liebe und ein tolles Familienessen 🥂"
date: 2024-09-29T19:02:20+01:00
draft: false
---


**Was für ein großartiger Tag!** Heute waren wir alle zusammen in einem asiatischen Restaurant, 
um einen ganz besonderen Anlass zu feiern. 🥢🥠 Oma und Opa hatten am 25.09. ihren Hochzeitstag – und zwar den 49.! 😲 
Das ist echt eine lange Zeit, sagte Papa, und nächstes Jahr muss es wohl eine noch größere Party geben! 🥳

Es war richtig schön, Oma und Opa zu sehen. Aber nicht nur die – auch Onkel Pawel, seine Frau Natascha und ihre Kinder Taissia und Nikita waren da! 👨‍👩‍👧‍👦 
Ich habe sie sooo lange nicht gesehen. Sogar Sonya, eine Freundin der Familie, war dabei. 💕 
Wir hatten eine richtig schöne Zeit zusammen und haben viel gelacht und geplaudert. 


### Ups und Downs – meine Stimmungsschwankungen 🍼

Vermutlich stecke ich in der nächsten Entwicklungsphase, weil ich manchmal etwas launisch bin. 😅 Von einer Sekunde auf die andere fange 
ich plötzlich an zu weinen und will dann auch nicht immer auf den Arm. 
Ich hoffe, meine Lieben sind nicht böse, wenn ich mal lieber für mich bleiben möchte. 🙈

Da ich schnell müde werde, sind wir nach dem Restaurant gleich nach Hause gefahren. 
Normalerweise gehen wir danach immer noch zu Oma und Opa, aber heute war ich einfach zu müde. 😴


### Ein Geschenk für Oma und Opa 🎁

Wir haben Oma und Opa eine Spülmaschine geschenkt, damit sie sich die Arbeit in der 
Küche ein bisschen leichter machen können. 🧼 
Jetzt muss Papa nur noch schauen, wie er sie anschließt! 💪🔧


### Meine Fortschritte 📈

Ich mache weiter Fortschritte und interessiere mich immer mehr für mein Spielzeug. 
Ich übe, Dinge in die Hand zu nehmen und von einer Hand in die andere zu übergeben. 🎲 
Das klappt noch nicht so ganz wie ich es mir vorstelle, aber ich komme dem Ziel näher! 🤗

Bis bald, ihr Lieben! :wave: :blue_heart:
