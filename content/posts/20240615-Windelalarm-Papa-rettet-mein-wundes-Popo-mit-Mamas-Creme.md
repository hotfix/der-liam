---
title: "Windelalarm! Papa Rettet Mein Wundes Popo Mit Mamas Creme"
date: 2024-06-15T20:18:52+02:00
draft: false
---

Hallo liebe Leser, heute war mal ein anstrengender Tag.

Als mein Papa mich heute Morgen wickelte, stellte er fest, dass mein Popo wund war.  Wahrscheinlich war ich deswegen sehr unruhig und habe meinen Eltern Sorgen gemacht.

Ich fühlte mich irgendwie nicht so wohl.

Als mein Papa es gemerkt hatte, hat er mein empfindliches Popo mit Creme eingecremt.  Leider hatten wir Zuhause noch nicht die richtige Creme, deswegen hat er die Mamas [Lansinoh Salbe](https://amzn.to/3VQGn0m)&midast; verwendet.  
Ich glaube, es hat schon ein bisschen geholfen. :baby: 
Mama sagte, dass die Hebamme am Montag vorbeikommt und wir sie dann fragen würden, welche Salbe wir uns besorgen können, damit mein Popo wieder schön sanftig rosa wird. Ich hoffe, dass mein Popo bald wieder heil ist.

Außerdem durfte Papa heute zum ersten Mal meine Bauchnabelschnur desinfizieren, davor hat das ja immer Mamas Hebamme gemacht.  Das hat er sehr vorsichtig gemacht, nicht dass er sich ausversehen abreisst. Der ist halt ein kleiner Angsthase. :medical_symbol:

Ansonsten waren wir wieder schön spazieren. :person_walking:


