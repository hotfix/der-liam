---
title: "Einiges Erlebt: Ostsee Trip Geschenke Und 3 Wochen Alt!"
date: 2024-06-29T14:05:01+02:00
draft: false
---

Wow, wie die Zeit vergeht! Ich habe mich schon seit fast einer Woche nicht mehr gemeldet. Es ist auch recht viel passiert.

Da das Wetter diese Woche mega heiß war, sind wir spontan an die Ostsee nach Travemünde gefahren. ☀️

Das war mein erster kurzer Urlaub und meine erste große Fahrt mit dem Auto.  Obwohl wir nicht lange da waren, hat mir die Reise super gefallen. Das Wetter war einfach der Hammer! Mit etwas Wind war es auch nicht zu heiß. ️

Auf dem Hin- und Rückweg habe ich übrigens super geschlafen.

Außerdem habe ich diese Woche meine Krankenkassenkarte zugeschickt bekommen und endlich kam auch meine Steuer-ID. So kann Papa jetzt Kindergeld für mich beantragen.

Es gab auch mega viele Geschenke diese Woche. Zum einen kam ein Paket von Lidl, wo Papa mich angemeldet hat. Ich bekam eine Menge Feuchttücher, ein paar Windeln, Pflegeöl und Creme für meine Wellnesszeit.

![Geschnek Sendung vom Lidl](Geschenk-Lidl-club_img1.jpg)

![Geschnek Sendung vom Lidl](Geschenk-Lidl-club_img2.jpg)

Außerdem kam eine Nachbarin vorbei und hat mir ein schickes Geschenk vorbeigebracht. Ich werde jetzt also voll stylisch sein.  
Papa sagt allerdings, dass es etwas zu warm ist und ich es erst anziehen kann, wenn es wieder etwas kühler wird (laut Wetter schon nächste Woche), oder Mama zieht es mir dann an.

![](Geschenk-Nachbahrin.jpg)

Ansonsten war Papa diese Woche viel unterwegs, so dass ich mit Mama alleine war. Es hat mir Spaß gemacht, sie war viel mit mir unterwegs.

Und das Coole ist: Ich bin gestern schon 3 Wochen alt geworden!  Nächsten Freitag ist dann mein nächster größerer Geburtstag!  Ich freue mich schon!