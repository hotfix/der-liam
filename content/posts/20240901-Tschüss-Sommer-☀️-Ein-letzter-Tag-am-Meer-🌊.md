---
title: Tschüss Sommer! ☀️ Ein letzter Tag am Meer 🌊
date: 2024-09-01T22:12:52+01:00
draft: false
---


**Heute ist schon der 1. September und wir haben immer noch tolles Sommerwetter!** :sun_with_face: 
Die Sonne strahlt und es ist richtig warm, fast wie im Hochsommer. :hot_face: 
Wir waren nochmal an der Ostsee und das war herrlich! 30 Grad Lufttemperatur und 19 Grad Wassertemperatur. 
Ich selbst war natürlich noch nicht baden, aber Mama war im Wasser und fand es echt toll! :swimmer:

Für unsere Ostseetrips hat Papa uns ein cooles [Strandzelt von Qeedo](https://amzn.to/40TXAt5)\* gekauft. :tent: 
Das ist mega praktisch, und Papa ist total begeistert, weil er es schnell auf- und abbauen kann. :thumbsup: Von Ihm gibts eine sichere Kaupfemfehlung!

Heute wird wohl der letzte Sommertag in diesem Jahr sein, und wir werden so schnell nicht mehr ans Meer fahren. :cry: 
Aber dieses Jahr waren wir schon ganz oft dort – richtig toll! :star_struck:


### Meine Entwicklung – kleine Schritte vorwärts :baby: :muscle:

Ich mache in meiner Entwicklung langsam aber sicher Fortschritte! :chart_with_upwards_trend: 
Ich versuche schon etwas länger, meinen Kopf zu halten – aber das klappt noch nicht ganz. :sweat_smile: 
Mein Kopf ist ganz schön schwer! :weight_lifting:

Aktuell finde ich auch meine Hände super spannend und spiele schon ein bisschen damit herum. :raised_hands: 
Wenn Papa oder Mama mir ein Spielzeug geben, halte ich es schon fest, aber ich kann noch nicht so viel damit machen. :baby_symbol:

Und ich lache jetzt auch immer mehr! :smile: Jeden Morgen schenke ich meinen lieben 
Eltern ein schönes Lächeln, damit der Tag gleich gut anfängt. :sunny:


Bis bald, ihr Lieben! :wave: :blue_heart:
