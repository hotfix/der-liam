---
title: "Mein Erster Monat Mit Geschenken Und Fotoshooting"
date: 2024-07-07T09:39:23+02:00
draft: false
---

Wow, heute ist schon wieder ein ganz besonderer Tag! Ich bin jetzt genau einen Monat alt geworden. Was für ein Datum, oder?

In so kurzer Zeit bin ich schon ein ganz schön großer Junge geworden. Mama und Papa sagen immer, ich wachse wie Unkraut.

Heute war auch Mamas Freundin Nastja zu Besuch. Sie wollte uns unbedingt sehen, bevor sie für ein paar Wochen ganz weit wegfliegt. 
Für Mama hat sie wunderschöne Gladiolen mitgebracht. Und für mich gab es natürlich auch mega tolle Geschenke von Hansekind!

Ich kann es kaum erwarten, mit all meinen neuen Spielsachen zu spielen. Aber erstmal muss ich mich ein bisschen ausruhen. So ein aufregender Tag macht ganz schön müde.

Nachmittags haben Mama und Papa sogar ein kleines Fotoshooting mit mir gemacht. Sie haben mich in dem tollen Geschenk unserer Nachbarschaft fotografiert. 
Ich bin schon gespannt, wie die Fotos aussehen!

Ansonsten haben wir den Tag ganz ruhig verbracht. Abends, bevor ich ins Bett gegangen bin, habe ich noch mit Oma und Opa telefoniert. 
Die haben sich so gefreut, meine Stimme zu hören.


P.S.: Ein großes Dankeschön an Nastja und unsere Nachbarschaft für die tollen Geschenke!